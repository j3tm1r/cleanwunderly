package com.jxhem.cleanarch.wdrivers.data

import com.jxhem.cleanarch.wdrivers.data.mapper.DriversMapper
import com.jxhem.cleanarch.wdrivers.data.repository.PersistentSource
import com.jxhem.cleanarch.wdrivers.data.store.PlacemarksDataStoreFactory
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PlacemarksRepository @Inject constructor(
  private val mapper: DriversMapper,
  private val factory: PlacemarksDataStoreFactory,
  private val persistentSource: PersistentSource
) : DriversRepository {
  override fun getPlacemark(name: String): Single<Placemark> =
    factory.getPersistentDataStore().getPlacemarkByName(name).map { mapper.mapFromEntity(it) }


  override fun getPlacemarks(): Single<List<Placemark>> {
    return persistentSource.arePlacemarksDownloaded()
      .flatMap { placemarksAvailable ->
        factory.getDataStore(placemarksAvailable)
          .getPLacemarks()
          .subscribeOn(Schedulers.io())
          .observeOn(Schedulers.io())
      }
      .flatMap { placemarks ->
        persistentSource.savePlacemarks(placemarks)
          .andThen(Single.just(placemarks.map { mapper.mapFromEntity(it) }))
      }
  }

  override fun savePlacemark(placemark: Placemark): Completable =
    persistentSource.savePlacemark(mapper.mapToEntity(placemark))

  override fun savePlacemarks(placemarks: List<Placemark>): Completable =
    persistentSource.savePlacemarks(placemarks.map { mapper.mapToEntity(it) })

}