package com.jxhem.cleanarch.wdrivers.data.mapper

import com.jxhem.cleanarch.wdrivers.data.model.CoordinatesEntity
import com.jxhem.cleanarch.wdrivers.data.model.PlacemarkEntity
import com.jxhem.cleanarch.wdrivers.domain.model.Coordinates
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import javax.inject.Inject

class DriversMapper @Inject constructor() : EntityMapper<PlacemarkEntity, Placemark> {
    override fun mapFromEntity(entity: PlacemarkEntity): Placemark =
        Placemark(
            address = entity.address,
            name = entity.name,
            interior = entity.interior,
            exterior = entity.exterior,
            vin = entity.vin,
            fuel = entity.fuel,
            engineType = entity.engineType,
            coordinates = Coordinates(
                entity.coordinates.lat,
                entity.coordinates.lon,
                entity.coordinates.z
            )
        )

    override fun mapToEntity(domain: Placemark): PlacemarkEntity =
        PlacemarkEntity(
            address = domain.address,
            name = domain.name,
            interior = domain.interior,
            exterior = domain.exterior,
            vin = domain.vin,
            fuel = domain.fuel,
            engineType = domain.engineType,
            coordinates = CoordinatesEntity(
                domain.coordinates.lat,
                domain.coordinates.lon,
                domain.coordinates.z
            )
        )
}