package com.jxhem.cleanarch.wdrivers.data.model

data class CoordinatesEntity(
    val lat: Float,
    val lon: Float,
    val z: Float
)