package com.jxhem.cleanarch.wdrivers.data.model

data class PlacemarkEntity(
    val name: String,
    val address: String,
    val coordinates: CoordinatesEntity,
    val exterior: String,
    val interior: String,
    val vin: String,
    val engineType: String,
    val fuel: Int
)