package com.jxhem.cleanarch.wdrivers.data.repository

import com.jxhem.cleanarch.wdrivers.data.model.PlacemarkEntity
import io.reactivex.Completable
import io.reactivex.Single

interface PersistentSource {
    fun savePlacemark(placemark: PlacemarkEntity): Completable
    fun savePlacemarks(placemarks: List<PlacemarkEntity>): Completable
    fun getPLacemarks(): Single<List<PlacemarkEntity>>
    fun getPlacemarkByName(name: String): Single<PlacemarkEntity>
    fun arePlacemarksDownloaded(): Single<Boolean>
}