package com.jxhem.cleanarch.wdrivers.data.repository

import com.jxhem.cleanarch.wdrivers.data.model.PlacemarkEntity
import io.reactivex.Single

interface RemoteSource {
    fun loadPlacemarks(): Single<List<PlacemarkEntity>>
}