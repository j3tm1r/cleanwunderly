package com.jxhem.cleanarch.wdrivers.data.store

import com.jxhem.cleanarch.wdrivers.data.repository.PlacemarksDataStore
import javax.inject.Inject

class PlacemarksDataStoreFactory @Inject constructor(
    private val persistentDataStore: PlacemarksPersistentDataStore,
    private val remoteDataStore: PlacemarksRemoteDataStore
) {
    fun getDataStore(arePLacemarksDownloaded: Boolean): PlacemarksDataStore =
        when (arePLacemarksDownloaded) {
            true -> persistentDataStore
            false -> remoteDataStore
        }

    fun getPersistentDataStore(): PlacemarksPersistentDataStore = persistentDataStore
}