package com.jxhem.cleanarch.wdrivers.data.store

import com.jxhem.cleanarch.wdrivers.data.model.PlacemarkEntity
import com.jxhem.cleanarch.wdrivers.data.repository.PersistentSource
import com.jxhem.cleanarch.wdrivers.data.repository.PlacemarksDataStore
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class PlacemarksPersistentDataStore @Inject constructor(
  private val persistentSource: PersistentSource
) : PlacemarksDataStore {
  override fun savePlacemarks(placemarks: List<PlacemarkEntity>): Completable =
    Completable.defer {
      persistentSource.savePlacemarks(placemarks)
    }

  override fun savePlacemark(placemarkEntity: PlacemarkEntity): Completable =
    Completable.defer {
      persistentSource.savePlacemark(placemarkEntity)
    }

  override fun getPlacemarkByName(name: String): Single<PlacemarkEntity> =
    persistentSource.getPlacemarkByName(name)

  override fun getPLacemarks(): Single<List<PlacemarkEntity>> =
    persistentSource.getPLacemarks()

}