package com.jxhem.cleanarch.wdrivers.data.store

import com.jxhem.cleanarch.wdrivers.data.model.PlacemarkEntity
import com.jxhem.cleanarch.wdrivers.data.repository.PlacemarksDataStore
import com.jxhem.cleanarch.wdrivers.data.repository.RemoteSource
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject

class PlacemarksRemoteDataStore @Inject constructor(
    private val remoteDataStore: RemoteSource
) : PlacemarksDataStore {
    override fun getPLacemarks(): Single<List<PlacemarkEntity>> =
        remoteDataStore.loadPlacemarks()

    override fun getPlacemarkByName(name: String): Single<PlacemarkEntity> =
        throw UnsupportedOperationException()

    override fun savePlacemark(placemarkEntity: PlacemarkEntity): Completable =
        throw UnsupportedOperationException("Saving is not implemented here...")

    override fun savePlacemarks(placemarks: List<PlacemarkEntity>): Completable =
        throw UnsupportedOperationException("Saving is not implemented here...")
}