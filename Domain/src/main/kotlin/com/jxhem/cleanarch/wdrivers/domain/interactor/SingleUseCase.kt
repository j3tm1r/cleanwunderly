package com.jxhem.cleanarch.wdrivers.domain.interactor

import com.jxhem.cleanarch.wdrivers.domain.executor.PostExecutionThread
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

abstract class SingleUseCase<T, in Params> constructor(
  private val postExecutionThread: PostExecutionThread
) {
  abstract fun buildUseCaseObservable(params: Params? = null): Single<T>

  private val disposables = CompositeDisposable()

  open fun execute(observer: DisposableSingleObserver<T>, params: Params? = null) {
    val observable = this.buildUseCaseObservable(params)
      .subscribeOn(Schedulers.io())
      .observeOn(postExecutionThread.scheduler)
    addDisposable(observable.subscribeWith(observer))
  }

  fun dispose() {
    disposables.dispose()
  }

  private fun addDisposable(disposable: Disposable) {
    disposables.addAll(disposable)
  }
}