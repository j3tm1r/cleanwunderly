package com.jxhem.cleanarch.wdrivers.domain.interactor.drivers

import com.jxhem.cleanarch.wdrivers.domain.executor.PostExecutionThread
import com.jxhem.cleanarch.wdrivers.domain.interactor.SingleUseCase
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository
import io.reactivex.Single
import javax.inject.Inject

class LoadDriversUseCase @Inject constructor(
  private val driversRepository: DriversRepository,
  private val postExecutionThread: PostExecutionThread
) : SingleUseCase<List<Placemark>, Nothing>(postExecutionThread) {
  override fun buildUseCaseObservable(params: Nothing?): Single<List<Placemark>> {
    return driversRepository.getPlacemarks()
  }
}