package com.jxhem.cleanarch.wdrivers.domain.interactor.drivers

import com.jxhem.cleanarch.wdrivers.domain.executor.PostExecutionThread
import com.jxhem.cleanarch.wdrivers.domain.interactor.CompletableUseCase
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository
import io.reactivex.Completable
import javax.inject.Inject

class SaveDriverUseCase @Inject constructor(
    private val driversRepository: DriversRepository,
    private val postExecutionThread: PostExecutionThread
) : CompletableUseCase<SaveDriverUseCase.Params>(postExecutionThread) {


    override fun buildUseCaseCompletable(params: Params?): Completable {
        if (params == null) throw IllegalArgumentException("Params can't be null")
        return driversRepository.savePlacemark(params.placemark)
    }

    data class Params constructor(val placemark: Placemark) {
        companion object {
            fun forLocation(placemark: Placemark): Params {
                return Params(placemark)
            }
        }
    }
}