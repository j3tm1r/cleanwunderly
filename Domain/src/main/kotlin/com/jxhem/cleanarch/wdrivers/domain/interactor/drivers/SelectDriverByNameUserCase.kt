package com.jxhem.cleanarch.wdrivers.domain.interactor.drivers

import com.jxhem.cleanarch.wdrivers.domain.executor.PostExecutionThread
import com.jxhem.cleanarch.wdrivers.domain.interactor.SingleUseCase
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository
import io.reactivex.Single
import javax.inject.Inject

class SelectDriverByNameUserCase @Inject constructor(
    private val repository: DriversRepository,
    private val postExecutionThread: PostExecutionThread
) : SingleUseCase<Placemark, SelectDriverByNameUserCase.Params>(postExecutionThread) {

    override fun buildUseCaseObservable(params: Params?): Single<Placemark> {
        if (params == null) throw IllegalArgumentException("Params can't be null")
        return repository.getPlacemark(params.driversName)
    }

    data class Params constructor(val driversName: String) {
        companion object {
            fun forDriverName(driversName: String): Params {
                return Params(driversName)
            }
        }
    }
}