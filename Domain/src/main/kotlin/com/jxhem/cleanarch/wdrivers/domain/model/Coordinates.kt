package com.jxhem.cleanarch.wdrivers.domain.model


data class Coordinates(
    val lat: Float,
    val lon: Float,
    val z: Float
) {
    companion object {
        fun fromArray(coordinates: List<Float>): Coordinates {
            return Coordinates(
                coordinates[1],
                coordinates[0],
                coordinates[2]
            )
        }
    }
}