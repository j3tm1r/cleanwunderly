package com.jxhem.cleanarch.wdrivers.domain.model

data class Placemark(
    val address: String,
    val coordinates: Coordinates,
    val interior: String,
    val exterior: String,
    val name: String,
    val vin: String,
    val engineType: String,
    val fuel: Int

) {

    fun getLat(): Double {
        return coordinates.lat.toDouble()
    }

    fun getLon(): Double {
        return coordinates.lon.toDouble()
    }
}