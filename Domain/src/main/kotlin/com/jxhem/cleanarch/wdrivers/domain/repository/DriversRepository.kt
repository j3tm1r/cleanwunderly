package com.jxhem.cleanarch.wdrivers.domain.repository

import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import io.reactivex.Completable
import io.reactivex.Single

interface DriversRepository {

  fun getPlacemark(name: String): Single<Placemark>
  fun getPlacemarks(): Single<List<Placemark>>
  fun savePlacemark(placemark: Placemark): Completable
  fun savePlacemarks(placemarks: List<Placemark>): Completable

  companion object {
    const val ERROR_MESSAGE = "Error while loading the drivers"
    const val LOADING_MESSAGE = "Loading drivers..."
  }
}