package com.jxhem.cleanarch.wdrivers.domain.interactor.drivers


import com.jxhem.cleanarch.wdrivers.domain.executor.PostExecutionThread
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository
import com.jxhem.cleanarch.wdrivers.domain.test.PlacemarksDataFactory
import io.reactivex.Observable
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class LoadDriversTest {

    private lateinit var loadDriversUseCase: LoadDriversUseCase

    @Mock
    lateinit var driversRepository: DriversRepository

    @Mock
    lateinit var postExecutionThread: PostExecutionThread

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        loadDriversUseCase = LoadDriversUseCase(
            driversRepository,
            postExecutionThread
        )
    }

    @After
    fun tearDown() {

    }

    @Test
    fun driversLoadingCompletes() {
        stubLoadDrivers(Observable.just(PlacemarksDataFactory.makePlacemarks(10)))
        val testObserver = loadDriversUseCase.buildUseCaseObservable().test()
        testObserver.assertComplete()
    }

    @Test
    fun placemarksReturnData() {
        val placemarks = PlacemarksDataFactory.makePlacemarks(10)
        stubLoadDrivers(Observable.just(placemarks))
        val testObserver = loadDriversUseCase.buildUseCaseObservable().test()
        testObserver.assertValue(placemarks)
    }

    private fun stubLoadDrivers(observable: Observable<List<Placemark>>) {
        doReturn(observable).`when`(driversRepository).getPlacemarks()
    }
}
