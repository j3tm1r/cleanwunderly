package com.jxhem.cleanarch.wdrivers.domain.interactor.drivers

import com.jxhem.cleanarch.wdrivers.domain.executor.PostExecutionThread
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository
import com.jxhem.cleanarch.wdrivers.domain.test.PlacemarksDataFactory
import io.reactivex.Completable
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@RunWith(JUnit4::class)
class SaveDriverUseCaseTest {

  @Mock
  lateinit var postExecutionThread: PostExecutionThread

  @Mock
  lateinit var driversRepository: DriversRepository

  private lateinit var saveDriverUseCase: SaveDriverUseCase

  @Before
  fun setUp() {
    MockitoAnnotations.initMocks(this)
    saveDriverUseCase = SaveDriverUseCase(
      driversRepository,
      postExecutionThread
    )
  }

  @After
  fun tearDown() {
  }

  @Test
  fun savePlacemarkCompletes() {
    stubPlacemarkCompletable(Completable.complete())
    val testObserver =
      saveDriverUseCase.buildUseCaseCompletable(
        SaveDriverUseCase.Params.forLocation(
          PlacemarksDataFactory.makePlacemark()
        )
      ).test()
    testObserver.assertComplete()
  }

  @Test(expected = IllegalArgumentException::class)
  fun savePlacemarkThrowsException() {
    saveDriverUseCase.buildUseCaseCompletable().test()
  }

  private fun stubPlacemarkCompletable(completable: Completable) {
    Mockito.`when`(
      driversRepository.savePlacemark(any())
    ).thenReturn(completable)
  }

  private fun <T> any(): T {
    Mockito.any<T>()
    return uninitialized()
  }

  private fun <T> uninitialized(): T = null as T
}