package com.jxhem.cleanarch.wdrivers.domain.test

import com.jxhem.cleanarch.wdrivers.domain.model.Coordinates
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import java.util.*
import kotlin.collections.ArrayList

object PlacemarksDataFactory {

    fun randomString(): String = UUID.randomUUID().toString()
    fun randomBoolean(): Boolean = Math.random() < 0.5

    fun makePlacemark(): Placemark =
        Placemark(
            address = randomString(),
            coordinates = Coordinates(0f, 0f, 0f),
            name = randomString(),
            fuel = 12,
            exterior = randomString(), interior = randomString(),
            engineType = randomString(), vin = randomString()
        )

    fun makePlacemarks(count: Int): List<Placemark> {
        val placemaarks = ArrayList<Placemark>()
        repeat(count) {
            placemaarks.add(makePlacemark())
        }
        return placemaarks
    }
}