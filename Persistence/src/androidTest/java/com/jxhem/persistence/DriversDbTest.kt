package com.jxhem.persistence

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import com.jxhem.cleanarch.wdrivers.persistence.db.WDriversDataBase
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DriversDbTest {

    private lateinit var driversDb: WDriversDataBase

    @Before
    fun initDb() {
        driversDb = Room.inMemoryDatabaseBuilder(
            InstrumentationRegistry.getContext(),
            WDriversDataBase::class.java
        ).build()
    }

    @After
    fun closeDb() {
        driversDb.close()
    }

    @Test
    fun insertAndLoad() {
        driversDb.wdriversDataDao().insertPlacemark(TestUtil.createPlacemark())
        val placemark = driversDb.wdriversDataDao().loadPlacemarks().blockingFirst()
        assertThat(placemark[0].name, `is`(TestUtil.createPlacemark().name))
    }
}