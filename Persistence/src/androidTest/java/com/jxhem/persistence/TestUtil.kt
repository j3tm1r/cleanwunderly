package com.jxhem.persistence

import com.jxhem.cleanarch.wdrivers.data.persistence.WdriversDBModel

object TestUtil {
    fun createPlacemark(): WdriversDBModel.DBPlaceMark = WdriversDBModel.DBPlaceMark(
        name = "name", address = "",
        vin = "",
        coordinates = WdriversDBModel.Coordinates(0f, 0f, 0f),
        engineType = "", exterior = "", interior = "", fuel = 1
    )
}
