package com.jxhem.cleanarch.wdrivers.persistence

import com.jxhem.cleanarch.wdrivers.data.model.PlacemarkEntity
import com.jxhem.cleanarch.wdrivers.data.repository.PersistentSource
import com.jxhem.cleanarch.wdrivers.persistence.db.WDriversDataBase
import com.jxhem.cleanarch.wdrivers.persistence.mapper.RoomMapper
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PersistedPlacemarksSource @Inject constructor(
  private val database: WDriversDataBase,
  private val roomMapper: RoomMapper
) : PersistentSource {

  override fun savePlacemarks(placemarks: List<PlacemarkEntity>): Completable =
    Completable.defer {
      database.wdriversDataDao()
        .insertPLacemarks(placemarks.map { roomMapper.mapToPersisted(it) })
      Completable.complete()
    }

  override fun getPLacemarks(): Single<List<PlacemarkEntity>> =
    database.wdriversDataDao().loadPlacemarks()
      .subscribeOn(Schedulers.io())
      .map { it.map { roomMapper.mapToDomain(it) } }

  override fun arePlacemarksDownloaded(): Single<Boolean> =
    database.wdriversDataDao().loadPlacemarks()
      .subscribeOn(Schedulers.io())
      .flatMap {
        Single.just(it.isNotEmpty())
      }

  override fun getPlacemarkByName(name: String): Single<PlacemarkEntity> =
    database.wdriversDataDao().loadPlacemark(name).map { roomMapper.mapToDomain(it) }

  override fun savePlacemark(placemark: PlacemarkEntity): Completable =
    Completable.defer {
      database.wdriversDataDao().insertPlacemark(roomMapper.mapToPersisted(placemark))
      Completable.complete()
    }
}