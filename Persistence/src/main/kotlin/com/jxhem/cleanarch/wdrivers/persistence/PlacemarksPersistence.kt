package com.jxhem.cleanarch.wdrivers.persistence

import android.content.Context
import com.jxhem.cleanarch.wdrivers.data.repository.PersistentSource
import com.jxhem.cleanarch.wdrivers.persistence.db.WDriversDataBase
import com.jxhem.cleanarch.wdrivers.persistence.mapper.RoomMapper


sealed class PlacemarksPersistence {

  companion object {
    fun providePersistedDataSource(context: Context): PersistentSource =
      PersistedPlacemarksSource(
        WDriversDataBase.getInstance(context)
        , RoomMapper()
      )
  }
}

