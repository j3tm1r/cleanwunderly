package com.jxhem.cleanarch.wdrivers.persistence.db

import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

object Model {
  @Entity(tableName = "placemarks", indices = [(Index("name"))])
  data class PlaceMark(
    @PrimaryKey val name: String,
    val address: String,
    @Embedded val coordinates: Coordinates,
    val exterior: String,
    val interior: String,
    val vin: String,
    val engineType: String,
    val fuel: Int
  )

  data class Coordinates(
    val lat: Float,
    val lon: Float,
    val z: Float
  )
}