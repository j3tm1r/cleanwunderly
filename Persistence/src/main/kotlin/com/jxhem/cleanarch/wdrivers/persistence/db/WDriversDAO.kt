package com.jxhem.cleanarch.wdrivers.persistence.db

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Single

@Dao
interface WDriversDAO {

  @Query("SELECT * FROM PLACEMARKS")
  fun loadPlacemarks(): Single<List<Model.PlaceMark>>

  @Query("SELECT * FROM PLACEMARKS WHERE NAME LIKE :name LIMIT 1")
  fun loadPlacemark(name: String): Single<Model.PlaceMark>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insertPlacemark(placemark: Model.PlaceMark)

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insertPLacemarks(placemarks: List<Model.PlaceMark>)

  @Query("DELETE FROM PLACEMARKS")
  fun deleteProjects()
}
