package com.jxhem.cleanarch.wdrivers.persistence.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(
  entities = [(Model.PlaceMark::class)],
  exportSchema = false,
  version = 1
)
abstract class WDriversDataBase : RoomDatabase() {
  abstract fun wdriversDataDao(): WDriversDAO

  companion object {
    private var INSTANCE: WDriversDataBase? = null

    fun getInstance(context: Context): WDriversDataBase {

      if (INSTANCE == null) {
        synchronized(WDriversDataBase::class) {
          INSTANCE = Room.databaseBuilder(
            context.applicationContext,
            WDriversDataBase::class.java, "wdrivers.db"
          ).fallbackToDestructiveMigration() // deletes all the db
            .build()
        }
      }
      return INSTANCE!!
    }

    fun destroyInstance() {
      INSTANCE = null
    }
  }
}