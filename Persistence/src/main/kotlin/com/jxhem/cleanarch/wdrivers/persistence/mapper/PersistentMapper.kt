package com.jxhem.cleanarch.wdrivers.persistence.mapper

interface PersistentMapper<P, E> {
    fun mapToDomain(persisted: P): E
    fun mapToPersisted(entity: E): P
}