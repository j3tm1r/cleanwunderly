package com.jxhem.cleanarch.wdrivers.persistence.mapper

import com.jxhem.cleanarch.wdrivers.data.model.CoordinatesEntity
import com.jxhem.cleanarch.wdrivers.data.model.PlacemarkEntity
import com.jxhem.cleanarch.wdrivers.persistence.db.Model

class RoomMapper : PersistentMapper<Model.PlaceMark, PlacemarkEntity> {
    override fun mapToDomain(persisted: Model.PlaceMark): PlacemarkEntity =
        PlacemarkEntity(
            address = persisted.address,
            name = persisted.name,
            coordinates = CoordinatesEntity(
                persisted.coordinates.lat,
                persisted.coordinates.lon,
                persisted.coordinates.z
            ),
            engineType = persisted.engineType,
            interior = persisted.interior,
            exterior = persisted.exterior,
            fuel = persisted.fuel,
            vin = persisted.vin
        )

    override fun mapToPersisted(entity: PlacemarkEntity): Model.PlaceMark =
        Model.PlaceMark(
            address = entity.address,
            name = entity.name,
            coordinates = Model.Coordinates(
                entity.coordinates.lat,
                entity.coordinates.lon,
                entity.coordinates.z
            ),
            engineType = entity.engineType,
            interior = entity.interior,
            exterior = entity.exterior,
            fuel = entity.fuel,
            vin = entity.vin
        )
}