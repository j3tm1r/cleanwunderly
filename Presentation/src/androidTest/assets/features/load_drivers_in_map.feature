# new feature
# Tags: optional
    
Feature: LoadDriversInMap
    Load the drivers in a Map
    
    Scenario Outline: Drivers are loaded into map and user position is shown
        Given I have a MainActivity
        When I click on the fab
        And I obtain the permissions
        And The CarsMapView is <visible>
        And A user location is fed
        Then The drivers have been loaded in the map
        And The users location is <visible>

    Examples:
        |visible|
        |true   |

    Scenario Outline: No drivers are loaded loading is shown
        Given I have a MainActivity
        When I click on the fab
        And I obtain the permissions
        And The CarsMapView is <visible>
        Then The loading message is <visible>

    Examples:
        |visible|
        |true   |

    Scenario Outline: Click on a driver icon
        Given I have a MainActivity
        And I click on the fab
        And The CarsMapView is <visible>
        And The drivers have been loaded in the map
        When I click on the <driver_number> driver
        Then A marker with text <displayed_name> for the <driver_number> driver is shown

    Examples:
        |driver_number|displayed_name|visible|
        |1            |HH-GO8002     |true   |

    Scenario Outline: Click on a driver icon when there is only one driver
        Given I have a MainActivity
        And I click on the fab
        And The CarsMapView is <visible>
        And The drivers have been loaded in the map
        And I click on the <driver_number> driver
        And A marker with text <displayed_name> for the <driver_number> driver is shown
        When I click on the <driver_number> driver
        Then The drivers have been loaded in the map

    Examples:
        |driver_number|displayed_name|visible|
        |1            |HH-GO8002     |true   |