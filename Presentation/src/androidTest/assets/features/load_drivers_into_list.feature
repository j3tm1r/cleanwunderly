Feature: LoadMoviesIntoList
    Load and show the drivers in a list

    Scenario Outline: Drivers are loaded into list
        Given I have a MainActivity
        When The CarsListView is <visible>
        Then The loading message is <visible>
        And The drivers have been loaded

    Examples:
        |visible|
        |true   |

    Scenario Outline: No drivers are loaded with error
        Given I have a MainActivity
        When I have a bad repository
        Then The error is <visible>

    Examples:
        |visible|
        |true   |