package com.jxhem.cleanarch.wdrivers.test

import android.app.Application
import android.content.Context
import android.os.Bundle
import android.support.test.runner.MonitoringInstrumentation
import com.jxhem.cleanarch.wdrivers.test.app.TestDriversApp
import cucumber.api.CucumberOptions

import cucumber.api.android.CucumberInstrumentationCore
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers


/***
 * Instrumentation class responsible for running the cucumber features tests
 */
@CucumberOptions(
    features = arrayOf("features"),
    glue = arrayOf("com.jxhem.cleanarch.wdrivers.test")
)
class Instrumentation : MonitoringInstrumentation() {
    private val instrumentationCore = CucumberInstrumentationCore(this)

    override fun onCreate(arguments: Bundle) {
        super.onCreate(arguments)
        instrumentationCore.create(arguments)
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        start()
    }

    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application {
        // we force the creation of a new Test application instead of te normal one
        return super.newApplication(cl, TestDriversApp::class.java.name, context)
    }

    override fun onStart() {
        super.onStart()
        waitForIdleSync()
        instrumentationCore.start()
    }
}