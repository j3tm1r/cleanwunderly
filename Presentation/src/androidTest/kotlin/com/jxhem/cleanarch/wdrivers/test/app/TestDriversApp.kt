package com.jxhem.cleanarch.wdrivers.test.app

import android.support.test.InstrumentationRegistry
import com.jxhem.cleanarch.wdrivers.presentation.app.BaseApp
import com.jxhem.cleanarch.wdrivers.test.di.DaggerTestAppComponent
import com.jxhem.cleanarch.wdrivers.test.di.TestAppComponent

class TestDriversApp : BaseApp() {

    private lateinit var appComponent: TestAppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerTestAppComponent.builder().application(this).build()
        appComponent.inject(this)

    }

    companion object {
        fun appComponent(): TestAppComponent =
            (InstrumentationRegistry.getTargetContext().applicationContext as TestDriversApp).appComponent

    }
}