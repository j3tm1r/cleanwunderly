package com.jxhem.cleanarch.wdrivers.test.di

import android.app.Application
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository
import com.jxhem.cleanarch.wdrivers.test.app.TestDriversApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        TestAppModule::class,
        TestDataModule::class
    ]
)
interface TestAppComponent {

    fun driversRepository(): DriversRepository

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): TestAppComponent.Builder

        fun build(): TestAppComponent
    }

    fun inject(application: TestDriversApp)
}