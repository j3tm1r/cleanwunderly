package com.jxhem.cleanarch.wdrivers.test.di

import android.app.Application
import android.content.Context
import com.jxhem.cleanarch.wdrivers.presentation.di.module.ActivityBuilder
import dagger.Binds
import dagger.Module

@Module(
    includes = [(ActivityBuilder::class)]
)
abstract class TestAppModule {

    @Binds
    abstract fun bindContext(application: Application): Context
}
