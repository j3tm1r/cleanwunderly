package com.jxhem.cleanarch.wdrivers.test.di

import com.jxhem.cleanarch.wdrivers.data.repository.PersistentSource
import com.jxhem.cleanarch.wdrivers.data.repository.RemoteSource
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository
import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock
import javax.inject.Singleton

@Module
class TestDataModule {
  @Provides
  @Singleton
  fun providePersistentSource(): PersistentSource = mock(PersistentSource::class.java)

  @Provides
  @Singleton
  fun provideDriversRepository(): DriversRepository = mock(DriversRepository::class.java)

  @Provides
  @Singleton
  fun provideRemoteDataSource(): RemoteSource = mock(RemoteSource::class.java)
}