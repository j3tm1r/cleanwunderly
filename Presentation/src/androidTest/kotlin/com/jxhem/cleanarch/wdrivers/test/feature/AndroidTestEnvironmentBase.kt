package com.jxhem.cleanarch.wdrivers.test.feature

import android.support.design.widget.Snackbar
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.rule.ActivityTestRule
import android.support.v4.app.Fragment
import com.jxhem.cleanarch.wdrivers.R
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository.Companion.ERROR_MESSAGE
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository.Companion.LOADING_MESSAGE
import com.jxhem.cleanarch.wdrivers.presentation.ui.activity.MainActivity
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.BaseView
import com.jxhem.cleanarch.wdrivers.test.app.TestDriversApp
import com.jxhem.cleanarch.wdrivers.test.util.PlacemarksDataFactory
import cucumber.api.CucumberOptions
import cucumber.api.java.After
import cucumber.api.java.Before
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import io.reactivex.Single
import junit.framework.Assert
import junit.framework.Assert.assertFalse
import org.hamcrest.Matchers
import org.junit.Rule
import org.mockito.Mockito.doReturn


@CucumberOptions(
  features = ["features"],
  glue = ["com.jxhem.wdrivers.test"]
)
class AndroidTestEnvironmentBase {

  @get:Rule
  val mainActivity = ActivityTestRule<MainActivity>(MainActivity::class.java, false, false)
  val placemarks = PlacemarksDataFactory.makeDomainPlacemarks()
  @Before
  fun setup() {
    stubDriversRepository(Single.just(placemarks))
    mainActivity.launchActivity(null)
  }

  @After
  fun tearDown() {
    mainActivity.finishActivity()
  }

  @Given("^I have a MainActivity$")
  fun i_have_a_MainActivity() = Assert.assertNotNull(mainActivity.activity)

  @Given("^I have a bad repository$")
  fun i_have_a_bad_repository() {
    stubDriversRepository(
      Single.error(Throwable("There was a problem loading drivers"))
    )
  }

  @When("^I click on the fab$")
  fun i_click_on_the_fab() {
    onView(withId(R.id.fab)).check(matches(isDisplayed()))
    onView(withId(R.id.fab)).perform(click())
  }

  @Then("^The error is (true|false)$")
  fun the_error_is_visible(visible: Boolean) {
    val visibility = when (visible) {
      true -> ViewMatchers.isCompletelyDisplayed()
      false -> Matchers.not(ViewMatchers.isCompletelyDisplayed())
    }

    ViewMatchers.withText(ERROR_MESSAGE).matches(
      Matchers.allOf(
        ViewMatchers.isDescendantOfA(
          ViewMatchers.isAssignableFrom(Snackbar.SnackbarLayout::class.java)
        ),
        visibility
      )
    )
  }

  @Then("^The loading message is (true|false)$")
  fun the_loading_message_is_visible(visible: Boolean) {
    val visibility = when (visible) {
      true -> ViewMatchers.isCompletelyDisplayed()
      false -> Matchers.not(ViewMatchers.isCompletelyDisplayed())
    }

    ViewMatchers.withText(LOADING_MESSAGE).matches(
      Matchers.allOf(
        ViewMatchers.isDescendantOfA(
          ViewMatchers.isAssignableFrom(Snackbar.SnackbarLayout::class.java)
        ),
        visibility
      )
    )
  }

  fun getDriversCount(): Int = placemarks.size

  fun checkViewIs(view: BaseView, visible: Boolean) {
    if (visible)
      assert(view.isVisible)
    else
      assertFalse(view.isVisible)
  }

  fun stubDriversRepository(single: Single<List<Placemark>>) {
    doReturn(single)
      .`when`(TestDriversApp.appComponent().driversRepository()).getPlacemarks()
  }

  fun stubLoadDriver(single: Single<Placemark>) {
    doReturn(single)
      .`when`(TestDriversApp.appComponent().driversRepository())
      .getPlacemark(org.mockito.Matchers.anyString())
  }

  fun findView(tag: String?): Fragment =
    mainActivity.activity.supportFragmentManager.findFragmentByTag(tag)

}