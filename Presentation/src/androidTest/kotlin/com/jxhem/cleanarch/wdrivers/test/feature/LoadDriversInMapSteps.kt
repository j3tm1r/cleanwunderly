package com.jxhem.cleanarch.wdrivers.test.feature

import android.os.Build
import android.support.test.InstrumentationRegistry.getInstrumentation
import android.support.test.uiautomator.UiDevice
import android.support.test.uiautomator.UiObject
import android.support.test.uiautomator.UiObjectNotFoundException
import android.support.test.uiautomator.UiSelector
import android.util.Log
import com.jxhem.cleanarch.wdrivers.R
import com.jxhem.cleanarch.wdrivers.presentation.Model
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.carsmap.CarsMapView
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import io.reactivex.Single
import junit.framework.Assert
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertTrue

class LoadDriversInMapSteps(
  private val testBase: AndroidTestEnvironmentBase
) {

  private val uiDevice = UiDevice.getInstance(getInstrumentation())

  private lateinit var carsMapView: CarsMapView

  @Given("^I obtain the permissions$")
  fun i_obtain_the_permissions() {
    allowPermissionsIfNeeded()
  }

  @When("^The CarsMapView is (true|false)$")
  fun the_CarsMapView_is_visible(visible: Boolean) {
    val view = testBase.findView(CarsMapView::class.java.simpleName)
    Assert.assertNotNull(view)
    carsMapView = view as CarsMapView
    testBase.checkViewIs(carsMapView, visible)
  }

  @When("^I click on the (\\d+) driver$")
  fun i_click_on_the_driver(driver_index: Int) {
    val marker = uiAutomatorLoadView(
      testBase.placemarks[driver_index].name
    )
    testBase.stubLoadDriver(
      Single.just(testBase.placemarks[driver_index])
    )
    assertNotNull(marker)
    assert(marker?.exists()!!)
    marker.click()
  }

  @Then("^The drivers have been loaded in the map$")
  fun the_drivers_have_been_loaded_in_the_map() {
    for (placemark in testBase.placemarks) {
      assert(uiAutomatorLoadView(placemark.name)?.exists()!!)
    }
  }


  @Then("^A marker with text (\\S+) for the (\\d+) driver is shown$")
  fun a_marker_with_text_for_the_selected_driver_is_shown(name: String, driver_index: Int) {
    val driverName = ""
    val marker = uiAutomatorLoadView(driverName)
    assertTrue(marker?.exists()!!)
  }


  @When("^A user location is fed$")
  fun a_user_location_is_fed() {
    carsMapView.onLocationUpdated(
      Model.CoordinatesView(
        testBase.placemarks[0].coordinates.lat, testBase.placemarks[0].coordinates.lon, 0f
      )
    )
  }


  @Then("^The users location is (true|false)$")
  fun the_users_location_is_visible(visible: Boolean) {
    var userMarker = uiAutomatorLoadView(
      carsMapView.resources.getString(R.string.users_location_label)
    )
    assertTrue(userMarker?.exists()!!)
  }

  private fun allowPermissionsIfNeeded() {
    if (Build.VERSION.SDK_INT >= 23) {
      val allowPermissions = uiDevice.findObject(UiSelector().text("ALLOW"))
      if (allowPermissions.exists()) {
        try {
          allowPermissions.click()
        } catch (e: UiObjectNotFoundException) {
          Log.e(
            this::class.java.simpleName,
            "There is no permissions dialog to interact with "
          )
        }

      }
    }
  }

  private fun uiAutomatorLoadView(driverName: String): UiObject? =
    uiDevice.findObject(
      UiSelector().descriptionContains(driverName)
    )
}