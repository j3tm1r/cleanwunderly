package com.jxhem.cleanarch.wdrivers.test.feature

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.matcher.ViewMatchers
import com.jxhem.cleanarch.wdrivers.R
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.carslist.CarsListView
import com.jxhem.cleanarch.wdrivers.test.util.RecyclerViewAssertions.adapterItemCountEquals
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import junit.framework.Assert.assertNotNull

class LoadDriversIntoListSteps(
  private val testBase: AndroidTestEnvironmentBase
) {

  private lateinit var carsListView: CarsListView

  @When("^There are no drivers loaded")
  fun no_drivers_loded() {
    onView(ViewMatchers.withId(R.id.drivers_list))
      .check(adapterItemCountEquals(0))
  }

  @Then("^The drivers have been loaded$")
  fun the_drivers_have_been_loaded() =
    onView(ViewMatchers.withId(R.id.drivers_list))
      .check(adapterItemCountEquals(testBase.getDriversCount()))


  @When("^The CarsListView is (true|false)$")
  fun the_CarsListView_is_visible(visible: Boolean) {
    val view = testBase.findView(CarsListView::class.java.simpleName)
    assertNotNull(view)
    carsListView = view as CarsListView
    testBase.checkViewIs(carsListView, visible)
  }
}
