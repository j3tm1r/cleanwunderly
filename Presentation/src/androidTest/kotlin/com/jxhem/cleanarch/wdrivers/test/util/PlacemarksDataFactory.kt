package com.jxhem.cleanarch.wdrivers.test.util

import com.jxhem.cleanarch.wdrivers.domain.model.Coordinates
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import com.jxhem.cleanarch.wdrivers.presentation.Model
import java.util.*
import kotlin.collections.ArrayList

object PlacemarksDataFactory {

  fun randomString(): String = UUID.randomUUID().toString()
  fun randomBoolean(): Boolean = Math.random() < 0.5

  fun makePlacemark(): Model.PlaceMarkView =
    Model.PlaceMarkView(
      address = randomString(),
      coordinates = Model.CoordinatesView(0f, 0f, 0f),
      name = randomString(),
      fuel = 12,
      exterior = randomString(), interior = randomString(),
      engineType = randomString(), vin = randomString()
    )

  fun makePlacemarks(count: Int): List<Model.PlaceMarkView> {
    val placemaarks = ArrayList<Model.PlaceMarkView>()
    repeat(count) {
      placemaarks.add(makePlacemark())
    }
    return placemaarks
  }

  fun makeDomainPlacemark(): Placemark =
    Placemark(
      address = randomString(),
      coordinates = Coordinates(
        (45.077f + Math.random() / 10).toFloat(),
        (7.666f + Math.random() / 10).toFloat(), 0f
      ),
      name = randomString(),
      fuel = 12,
      exterior = randomString(),
      interior = randomString(),
      engineType = randomString(),
      vin = randomString()
    )

  fun makeDomainPlacemarks(): List<Placemark> {
    val placemaarks = ArrayList<Placemark>()

    placemaarks.add(
      Placemark(
        address = "Lesserstraße 170, 22049 Hamburg",
        coordinates = Coordinates(53.59301f, 10.07526f, 0f),
        engineType = "CE",
        exterior = "UNACCEPTABLE",
        fuel = 42,
        interior = "UNACCEPTABLE",
        name = "HH-GO8522",
        vin = "WME4513341K565439"
      )
    )

    placemaarks.add(
      Placemark(
        address = "Ring 2, 22043 Hamburg",
        coordinates = Coordinates(53.56388f, 10.07838f, 0f),
        engineType = "CE",
        exterior = "GOOD",
        fuel = 84,
        interior = "GOOD",
        name = "HH-GO8002",
        vin = "WME4513341K412713"
      )
    )

    placemaarks.add(
      Placemark(
        address = "Grosse Reichenstraße 7, 20457 Hamburg",
        coordinates = Coordinates(53.54847f, 9.99622f, 0f),
        engineType = "CE",
        exterior = "UNACCEPTABLE",
        fuel = 45,
        interior = "GOOD",
        name = "HH-GO8480",
        vin = "WME4513341K412697"
      )
    )
    placemaarks.add(
      Placemark(
        address = "Spreenende 1 - 11, 22453 Hamburg (Umkreis 100m)",
        coordinates = Coordinates(53.61274f, 9.97417f, 0f),
        engineType = "CE",
        exterior = "UNACCEPTABLE",
        fuel = 57,
        interior = "UNACCEPTABLE",
        name = "HH-GO8001",
        vin = "WME4513341K412709"
      )
    )

    return placemaarks
  }
}