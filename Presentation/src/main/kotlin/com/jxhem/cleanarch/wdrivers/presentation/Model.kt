package com.jxhem.cleanarch.wdrivers.presentation

object Model {
  data class PlaceMarkView(
    val name: String,
    val address: String,
    val coordinates: CoordinatesView,
    val exterior: String,
    val interior: String,
    val vin: String,
    val engineType: String,
    val fuel: Int
  ) {
    fun getLat(): Double {
      return coordinates.lat.toDouble()
    }

    fun getLon(): Double {
      return coordinates.lon.toDouble()
    }
  }

  data class CoordinatesView(
    val lat: Float,
    val lon: Float,
    val z: Float
  )
}