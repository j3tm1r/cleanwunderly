package com.jxhem.cleanarch.wdrivers.presentation.android.adapter

import android.databinding.BindingAdapter
import android.widget.TextView
import com.jxhem.cleanarch.wdrivers.R
import com.jxhem.cleanarch.wdrivers.presentation.Model

object BindingAdapters {

  /***
   * [BindigAdapter] responsible for formatting the text for the coordinates textview
   */
  @JvmStatic
  @BindingAdapter("placemarkCoordinates")
  fun coordinatesText(view: TextView, coordinates: Model.CoordinatesView) {
    view.text = String.format(
      view.context.resources.getString(R.string.coordinates_interpolator),
      coordinates.lat,
      coordinates.lon
    )
  }

  /***
   * [BindigAdapter] responsible for formatting the text that describes a condition
   */
  @JvmStatic
  @BindingAdapter("vehicleCondition")
  fun vehicleCondition(view: TextView, condition: String) {
    Condition.valueOf(condition).let {
      when (it) {
        Condition.GOOD -> view.setBackgroundColor(
          view.context.resources.getColor(R.color.good_state)
        )
        Condition.UNACCEPTABLE -> view.setBackgroundColor(
          view.context.resources.getColor(R.color.bad_state)
        )
      }
    }
  }

  enum class Condition {
    GOOD,
    UNACCEPTABLE
  }
}