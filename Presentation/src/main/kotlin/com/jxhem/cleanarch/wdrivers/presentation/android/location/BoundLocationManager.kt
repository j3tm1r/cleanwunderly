package com.jxhem.cleanarch.wdrivers.presentation.android.location

import android.Manifest
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.LifecycleOwner
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import com.jxhem.cleanarch.wdrivers.presentation.Model

class BoundLocationManager private constructor(
    private val lifecycleOwner: LifecycleOwner,
    private val listener: LocationUpdater,
    private val context: Context
) : LifecycleObserver {
    private var locationManager: LocationManager? = null

    interface LocationUpdater {
        fun onLocationUpdated(location: Model.CoordinatesView)
        fun requestPermissions()
    }

    init {
        this.lifecycleOwner.lifecycle.addObserver(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun addLocationListener() {
        locationManager =
                context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        locationManager.let {
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) listener.requestPermissions()
            else {
                it?.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    5000, 30f,
                    mLocationListener
                )
            }
        }
    }


    private val mLocationListener: LocationListener = object : LocationListener {


        override fun onLocationChanged(location: Location?) {
            if (location != null) {
                this@BoundLocationManager.listener.onLocationUpdated(
                    Model.CoordinatesView(
                        location.latitude.toFloat(),
                        location.longitude.toFloat(),
                        0f
                    )
                )
            }
        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            // we could handle changes of the location feature here
        }

        override fun onProviderEnabled(provider: String?) {
            // provider is now available
        }

        override fun onProviderDisabled(provider: String?) {
            // provider was disabled
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun removeLocationListener() {
        if (locationManager == null) {
            return
        }
        locationManager!!.removeUpdates(mLocationListener)
        locationManager = null
    }

    companion object {
        fun create(
            lifecycleOwner: LifecycleOwner, listener: LocationUpdater, context: Context
        ): BoundLocationManager {
            return BoundLocationManager(
                lifecycleOwner,
                listener,
                context
            )
        }
    }
}