package com.jxhem.cleanarch.wdrivers.presentation.android.location

import android.location.Location

interface LocationManager {
    fun updateUsersLocation(location: Location)
}