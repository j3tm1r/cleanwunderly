package com.jxhem.cleanarch.wdrivers.presentation.app

import com.jxhem.cleanarch.wdrivers.presentation.di.DaggerAppComponent

class DriversApp : BaseApp() {
  override fun onCreate() {
    super.onCreate()
    DaggerAppComponent
      .builder()
      .application(this)
      .build()
      .inject(this)
  }
}