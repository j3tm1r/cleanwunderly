package com.jxhem.cleanarch.wdrivers.presentation.di

import android.app.Application
import com.jxhem.cleanarch.wdrivers.presentation.app.DriversApp
import com.jxhem.cleanarch.wdrivers.presentation.di.module.AppModule
import com.jxhem.cleanarch.wdrivers.presentation.di.module.DataModule
import com.jxhem.cleanarch.wdrivers.presentation.di.module.PersistenceModule
import com.jxhem.cleanarch.wdrivers.presentation.di.module.RemoteModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
  modules = [
    AndroidInjectionModule::class,
    AppModule::class,
    PersistenceModule::class,
    RemoteModule::class,
    DataModule::class
  ]
)
interface AppComponent {

  @Component.Builder
  interface Builder {
    @BindsInstance
    fun application(application: Application): AppComponent.Builder

    fun build(): AppComponent
  }

  fun inject(app: DriversApp)
}