package com.jxhem.cleanarch.wdrivers.presentation.di

/**
 * Helper interface to detect objects that are injectable by dagger
 */
interface Injectable