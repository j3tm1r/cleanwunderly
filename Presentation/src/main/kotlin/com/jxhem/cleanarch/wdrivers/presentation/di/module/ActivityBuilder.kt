package com.jxhem.cleanarch.wdrivers.presentation.di.module

import com.jxhem.cleanarch.wdrivers.presentation.ui.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(
  includes = [(ActivityModule::class)]
)
abstract class ActivityBuilder {

  @ContributesAndroidInjector(
    modules = [(PresentationModule::class), (ActivityModule::class)]
  )
  abstract fun contributeMainActivity(): MainActivity
}