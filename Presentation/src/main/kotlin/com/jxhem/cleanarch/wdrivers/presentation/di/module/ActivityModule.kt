package com.jxhem.cleanarch.wdrivers.presentation.di.module

import com.jxhem.cleanarch.wdrivers.presentation.ui.activity.MainActivity
import com.jxhem.cleanarch.wdrivers.presentation.ui.navigation.NavigationController
import com.jxhem.cleanarch.wdrivers.presentation.ui.navigation.NavigationProvider
import dagger.Module
import dagger.Provides

@Module
class ActivityModule {

  @Provides
  fun provideNavigationProvider(
    activity: MainActivity
  ): NavigationProvider = NavigationController(activity)

}