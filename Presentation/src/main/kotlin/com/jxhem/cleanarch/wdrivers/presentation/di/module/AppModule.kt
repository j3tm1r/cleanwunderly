package com.jxhem.cleanarch.wdrivers.presentation.di.module

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module


@Module(
  includes = [(ActivityBuilder::class)]
)
abstract class AppModule {
  @Binds
  abstract fun bindContext(application: Application): Context
}

