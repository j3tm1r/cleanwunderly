package com.jxhem.cleanarch.wdrivers.presentation.di.module

import com.jxhem.cleanarch.wdrivers.data.PlacemarksRepository
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository
import dagger.Binds
import dagger.Module

@Module
abstract class DataModule {
  @Binds
  abstract fun bindDriversRepository(placemarksRepository: PlacemarksRepository): DriversRepository
}