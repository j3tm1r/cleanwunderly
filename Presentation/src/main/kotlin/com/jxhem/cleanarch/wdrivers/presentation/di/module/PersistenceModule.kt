package com.jxhem.cleanarch.wdrivers.presentation.di.module

import android.content.Context
import com.jxhem.cleanarch.wdrivers.data.repository.PersistentSource
import com.jxhem.cleanarch.wdrivers.persistence.PlacemarksPersistence
import dagger.Module
import dagger.Provides

@Module
abstract class PersistenceModule {

  @Module
  companion object {
    @Provides
    @JvmStatic
    fun providePersistentSource(context: Context): PersistentSource =
      PlacemarksPersistence.providePersistedDataSource(context = context)
  }
}