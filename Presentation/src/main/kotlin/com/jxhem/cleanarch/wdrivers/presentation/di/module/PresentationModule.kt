package com.jxhem.cleanarch.wdrivers.presentation.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.jxhem.cleanarch.wdrivers.domain.executor.PostExecutionThread
import com.jxhem.cleanarch.wdrivers.presentation.app.UiThread
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.carslist.CarsListView
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.carslist.CarsListViewModel
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.carsmap.CarsMapView
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.carsmap.CarsMapViewModel
import com.jxhem.cleanarch.wdrivers.presentation.viewmodel.ViewModelFactory
import com.jxhem.cleanarch.wdrivers.presentation.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap


@Module
abstract class PresentationModule {
  @Binds
  abstract fun bindPostExecutionThread(uiThread: UiThread): PostExecutionThread

  @ContributesAndroidInjector
  abstract fun contributeCarsListView(): CarsListView

  @ContributesAndroidInjector
  abstract fun contributeCarsMapView(): CarsMapView

  @Binds
  @IntoMap
  @ViewModelKey(CarsListViewModel::class)
  abstract fun bindCarsListViewModel(carsListViewModel: CarsListViewModel): ViewModel

  @Binds
  @IntoMap
  @ViewModelKey(CarsMapViewModel::class)
  abstract fun bindCarsMapViewModel(carsMapViewModel: CarsMapViewModel): ViewModel

  @Binds
  abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}