package com.jxhem.cleanarch.wdrivers.presentation.di.module

import com.jxhem.cleanarch.wdrivers.data.repository.RemoteSource
import com.jxhem.cleanarch.wdrivers.remote.NetModule
import com.jxhem.cleanarch.wdrivers.remote.RemotePlacemarksSource
import com.jxhem.cleanarch.wdrivers.remote.mapper.RemoteMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteModule {
  @Provides
  @Singleton
  fun provideRemoteDataSource(): RemoteSource {
    return RemotePlacemarksSource(
      NetModule.provideLocationsApi(),
      RemoteMapper()
    )
  }
}