package com.jxhem.cleanarch.wdrivers.presentation.mapper

interface Mapper<out V, in D> {
  fun mapToView(dataPlacemark: D): V
}