package com.jxhem.cleanarch.wdrivers.presentation.mapper

import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import com.jxhem.cleanarch.wdrivers.presentation.Model
import javax.inject.Inject

class PlacemarksViewMapper @Inject constructor(

) : Mapper<Model.PlaceMarkView, Placemark> {
  override fun mapToView(dataPlacemark: Placemark): Model.PlaceMarkView =
    Model.PlaceMarkView(
      address = dataPlacemark.address,
      name = dataPlacemark.name,
      interior = dataPlacemark.interior,
      exterior = dataPlacemark.exterior,
      vin = dataPlacemark.vin,
      fuel = dataPlacemark.fuel,
      engineType = dataPlacemark.engineType,
      coordinates = Model.CoordinatesView(
        dataPlacemark.coordinates.lat,
        dataPlacemark.coordinates.lon,
        dataPlacemark.coordinates.z
      )
    )
}