package com.jxhem.cleanarch.wdrivers.presentation.ui.activity

import android.os.Bundle
import com.jxhem.cleanarch.wdrivers.R
import com.jxhem.cleanarch.wdrivers.presentation.ui.navigation.NavigationProvider
import javax.inject.Inject

class MainActivity : BaseActivity() {

  @Inject
  lateinit var navigationProvider: NavigationProvider

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_drivers)
    if (savedInstanceState == null)
      navigationProvider.navigateToListView()
  }
}