package com.jxhem.cleanarch.wdrivers.presentation.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.jxhem.cleanarch.wdrivers.databinding.PlacemarkLayoutBinding
import com.jxhem.cleanarch.wdrivers.presentation.Model

class DriverViewHolder(private val viewBinding: PlacemarkLayoutBinding) :
  RecyclerView.ViewHolder(viewBinding.root) {

  fun bind(placemark: Model.PlaceMarkView) {
    viewBinding.placemark = placemark
  }

  companion object {
    fun create(parent: ViewGroup): DriverViewHolder {
      val view = PlacemarkLayoutBinding.inflate(
        LayoutInflater.from(parent.context),
        parent,
        false
      )
      return DriverViewHolder(view)
    }
  }
}