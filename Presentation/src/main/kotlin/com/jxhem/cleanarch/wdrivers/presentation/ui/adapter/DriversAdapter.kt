package com.jxhem.cleanarch.wdrivers.presentation.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.jxhem.cleanarch.wdrivers.presentation.Model

class DriversAdapter(
  private var movies: List<Model.PlaceMarkView>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
    return DriverViewHolder.create(parent)
  }

  fun updateMovies(newMovies: List<Model.PlaceMarkView>) {
    this.movies = newMovies
    notifyDataSetChanged()
  }

  override fun getItemCount(): Int {
    return movies.size
  }

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    (holder as? DriverViewHolder)?.bind(movies[position])
  }
}