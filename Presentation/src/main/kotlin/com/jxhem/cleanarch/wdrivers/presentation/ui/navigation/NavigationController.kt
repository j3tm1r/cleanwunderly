package com.jxhem.cleanarch.wdrivers.presentation.ui.navigation

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.jxhem.cleanarch.wdrivers.R
import com.jxhem.cleanarch.wdrivers.presentation.ui.activity.MainActivity
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.BaseView
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.carslist.CarsListView
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.carsmap.CarsMapView


class NavigationController constructor(baseActivity: MainActivity) : NavigationProvider {
  private val fragmentManager: FragmentManager = baseActivity.supportFragmentManager
  private val containerId: Int = R.id.container


  override fun navigateToListView() {
    val listView = (findFragment(CarsListView::class.java.simpleName) ?: CarsListView.create())
    showView(listView as BaseView)
  }

  override fun navigateToMapView() {
    val mapView = findFragment(CarsMapView::class.java.simpleName) ?: CarsMapView.create()
    showView(mapView as BaseView)
  }

  private fun showView(view: BaseView) {
    fragmentManager
      .beginTransaction()
      .replace(containerId, view, view::class.java.simpleName)
      .setReorderingAllowed(true)
      .commit()
  }

  private fun removeFragment(id: String) {
    var fragment = fragmentManager.findFragmentByTag(id)
    if (fragment != null) fragmentManager.beginTransaction().remove(fragment).commit()
  }

  private fun findFragment(id: String): Fragment? {
    return fragmentManager.findFragmentByTag(id)
  }

}