package com.jxhem.cleanarch.wdrivers.presentation.ui.navigation


interface NavigationProvider {

  fun navigateToListView()

  fun navigateToMapView()
}