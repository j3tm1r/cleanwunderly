package com.jxhem.cleanarch.wdrivers.presentation.ui.views

import android.arch.lifecycle.ViewModelProvider
import android.content.pm.PackageManager
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.View
import com.jxhem.cleanarch.wdrivers.R
import com.jxhem.cleanarch.wdrivers.presentation.di.Injectable
import javax.inject.Inject

/**
 *  Base class for our view system. It implements [Injectable]
 *  in order for [Dagger] to correctly inject these fragments.
 *  It extends [Fragment] as our views are based on fragments.
 *
 *  It provides two helper methods to deal with the [Snackbar] Android widget.
 */
abstract class BaseView : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    fun showMessage(view: View, message: String) =
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT)
            .show()

    fun showMessageWithAction(view: View, message: String, onClickListener: View.OnClickListener) =
        Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.retry, onClickListener)
            .show()


    private var permissionCode: Int = 0
    private lateinit var permissions: Array<String>
    private lateinit var permissionListener: PermissionsListener


    /**
     * Check if the [permission] has been granted by the OS
     * */
    fun permissionGranted(permission: String) = ActivityCompat.checkSelfPermission(
        context!!,
        permission
    ) != PackageManager.PERMISSION_GRANTED

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == permissionCode &&
            grantResults.any { it != PackageManager.PERMISSION_GRANTED }
        ) {
            requestPermissions(permissions, permissionCode)
        } else {
            //permissions correctly granted
            permissionListener.onPermissionsReceived(requestCode)
        }
    }

    fun askForPermissions(request_code: Int, listener: PermissionsListener) {
        permissionCode = request_code
        permissionListener = listener
        requestPermissions(listener.permissions, request_code)
    }

    fun weHavePermissions(listener: PermissionsListener): Boolean {
        listener.permissions.forEach { if (!permissionGranted(it)) return false }
        return true
    }

    interface PermissionsListener {
        fun onPermissionsReceived(requestCode: Int)
        val permissions: Array<String>
    }
}