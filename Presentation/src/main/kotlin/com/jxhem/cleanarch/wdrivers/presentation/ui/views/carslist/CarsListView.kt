package com.jxhem.cleanarch.wdrivers.presentation.ui.views.carslist

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.jxhem.cleanarch.wdrivers.R
import com.jxhem.cleanarch.wdrivers.presentation.state.ResourceState
import com.jxhem.cleanarch.wdrivers.presentation.ui.adapter.DriversAdapter
import com.jxhem.cleanarch.wdrivers.presentation.ui.navigation.NavigationProvider
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.BaseView
import kotlinx.android.synthetic.main.content_drivers.*
import javax.inject.Inject


class CarsListView : BaseView() {

  @Inject
  lateinit var navigationController: NavigationProvider

  private lateinit var viewModel: CarsListViewModel
  private lateinit var rootView: RelativeLayout

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    rootView = inflater.inflate(
      R.layout.content_drivers, container, false
    ) as RelativeLayout
    return rootView
  }

  override fun onStart() {
    super.onStart()
    setupPlacemarks()
  }

  override fun onActivityCreated(savedInstanceState: Bundle?) {
    super.onActivityCreated(savedInstanceState)
    viewModel = ViewModelProviders.of(this, viewModelFactory).get(CarsListViewModel::class.java)

    drivers_list.adapter = DriversAdapter(emptyList())
    fab.setOnClickListener {
      navigationController.navigateToMapView()
    }
  }

  private fun setupPlacemarks() {
    viewModel.getPlacemarks()
      .observe(this, Observer {
        when (it?.status) {
          ResourceState.SUCCESS -> {
            (drivers_list.adapter as DriversAdapter).updateMovies(it?.data!!)
          }
          ResourceState.LOADING -> { showMessage(rootView, it?.message!!) }
          else -> {
            showMessageWithAction(rootView, it?.message!!,
              View.OnClickListener { viewModel.loadPlacemarks() }
            )
          }
        }
      })
    viewModel.loadPlacemarks()
  }

  companion object {
    fun create(): CarsListView {
      return CarsListView()
    }
  }
}