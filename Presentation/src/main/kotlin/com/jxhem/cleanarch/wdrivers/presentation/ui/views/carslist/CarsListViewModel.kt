package com.jxhem.cleanarch.wdrivers.presentation.ui.views.carslist

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.jxhem.cleanarch.wdrivers.domain.interactor.drivers.LoadDriversUseCase
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository.Companion.LOADING_MESSAGE
import com.jxhem.cleanarch.wdrivers.presentation.Model
import com.jxhem.cleanarch.wdrivers.presentation.mapper.PlacemarksViewMapper
import com.jxhem.cleanarch.wdrivers.presentation.state.Resource
import com.jxhem.cleanarch.wdrivers.testing.OpenForTesting
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@OpenForTesting
class CarsListViewModel
@Inject constructor(
  private val loadDriversUseCase: LoadDriversUseCase,
  private val mapper: PlacemarksViewMapper
) : ViewModel() {

  private val placemarks: MutableLiveData<Resource<List<Model.PlaceMarkView>>> =
    MutableLiveData()

  fun getPlacemarks(): LiveData<Resource<List<Model.PlaceMarkView>>> {
    return placemarks
  }

  fun loadPlacemarks() {
    placemarks.postValue(Resource.loading(LOADING_MESSAGE))
    return loadDriversUseCase.execute(PlacemarksSubscriber())
  }

  inner class PlacemarksSubscriber : DisposableSingleObserver<List<Placemark>>() {
    override fun onSuccess(t: List<Placemark>) {
      placemarks.postValue(Resource.success(t.map { mapper.mapToView(it) }))
    }

    override fun onError(e: Throwable) {
      placemarks.postValue(Resource.error(e.localizedMessage))
    }
  }
}

