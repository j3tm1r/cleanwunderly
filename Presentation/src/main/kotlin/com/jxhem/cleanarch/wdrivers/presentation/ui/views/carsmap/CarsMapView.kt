package com.jxhem.cleanarch.wdrivers.presentation.ui.views.carsmap

import android.Manifest
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.SupportMapFragment
import com.jxhem.cleanarch.wdrivers.R
import com.jxhem.cleanarch.wdrivers.presentation.Model
import com.jxhem.cleanarch.wdrivers.presentation.android.location.BoundLocationManager
import com.jxhem.cleanarch.wdrivers.presentation.ui.navigation.NavigationProvider
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.BaseView
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.MapClickListener
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.MapWrapper
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.google.IMapManagerGMap
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state.ErrorState
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state.LoadingState
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state.MapState
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.strategy.MoveOnUserClickedStrategy
import kotlinx.android.synthetic.main.content_drivers_map.*
import javax.inject.Inject

class CarsMapView : BaseView(), BaseView.PermissionsListener
    , BoundLocationManager.LocationUpdater, MapClickListener {

    override val permissions: Array<String>
        get() = arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

    @Inject
    lateinit var navigationController: NavigationProvider

    private lateinit var viewModel: CarsMapViewModel
    private lateinit var rootView: ConstraintLayout
    private var mapManager: MapWrapper? = null
    private lateinit var locationManager: BoundLocationManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(
            R.layout.content_drivers_map, container, false
        ) as ConstraintLayout
        return rootView
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(CarsMapViewModel::class.java)
        fab.setOnClickListener { navigationController.navigateToListView() }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        locationManager = BoundLocationManager.create(this, this, context!!)
    }

    override fun onAttachFragment(childFragment: Fragment?) {
        super.onAttachFragment(childFragment)
        /*
        * Since our fragment is inflated through the xml we should be waiting
        * for it to be attached as this fragments' children
        */
        (childFragment as SupportMapFragment).let {
            it.getMapAsync {
                MoveOnUserClickedStrategy.Builder()
                    .setUserSelectedSource(viewModel.placemark)
                    .setPlacemarksSource(viewModel.locations)
                    .setStateHolder(viewModel.mapState)
                    .build()

                mapManager = IMapManagerGMap(it, this.context!!, this)
                // we chechk for the locationPermissions
                // if we have them we setup everything
                if (!weHavePermissions(this)) askForPermissions(PERMISSION_CODE, this)
                else restoreDriverIfPresent()
            }
        }
    }

    private fun restoreDriverIfPresent() {
        viewModel.mapState.observeForever {
            when (it) {
                is ErrorState -> showMessageWithAction(
                    rootView, it.getMessage(),
                    View.OnClickListener { viewModel.loadPlacemarks() }
                )
                is LoadingState -> showMessage(rootView, it.getMessage())
                else -> renderState(it!!)
            }
        }
        viewModel.loadPlacemarks()
    }

    override fun onDriverSelected(name: String) {
        viewModel.selectDriverByName(name)
    }

    override fun onLocationUpdated(location: Model.CoordinatesView) {
        rootView.post {
            mapManager?.updateUsersLocation(location)
        }
    }

    override fun onPermissionsReceived(requestCode: Int) {
        if (requestCode == PERMISSION_CODE) {
            restoreDriverIfPresent()
        }
    }

    override fun requestPermissions() = askForPermissions(PERMISSION_CODE, this)

    private fun renderState(state: MapState) = mapManager?.renderState(state)

    companion object {
        private const val PERMISSION_CODE = 173

        fun create(): CarsMapView {
            return CarsMapView()
        }
    }
}
