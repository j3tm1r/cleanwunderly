package com.jxhem.cleanarch.wdrivers.presentation.ui.views.carsmap

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.jxhem.cleanarch.wdrivers.domain.interactor.drivers.LoadDriversUseCase
import com.jxhem.cleanarch.wdrivers.domain.interactor.drivers.SelectDriverByNameUserCase
import com.jxhem.cleanarch.wdrivers.domain.model.Placemark
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository.Companion.ERROR_MESSAGE
import com.jxhem.cleanarch.wdrivers.domain.repository.DriversRepository.Companion.LOADING_MESSAGE
import com.jxhem.cleanarch.wdrivers.presentation.Model
import com.jxhem.cleanarch.wdrivers.presentation.mapper.PlacemarksViewMapper
import com.jxhem.cleanarch.wdrivers.presentation.state.Resource
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state.MapState
import com.jxhem.cleanarch.wdrivers.testing.OpenForTesting
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

@OpenForTesting
class CarsMapViewModel @Inject constructor(
    private val loadDriversUseCase: LoadDriversUseCase,
    private val selectDriverByNameUserCase: SelectDriverByNameUserCase,
    private val mapper: PlacemarksViewMapper
) : ViewModel() {

    val locations = MutableLiveData<Resource<List<Model.PlaceMarkView>>>()
    val placemark = MutableLiveData<Resource<Model.PlaceMarkView>>()
    val mapState = MediatorLiveData<MapState>()

    override fun onCleared() {
        loadDriversUseCase.dispose()
        selectDriverByNameUserCase.dispose()
        super.onCleared()
    }

    fun loadPlacemarks() {
        locations.postValue(Resource.loading(LOADING_MESSAGE))
        return loadDriversUseCase.execute(PlacemarksSubscriber())
    }

    fun selectDriverByName(name: String) {
        selectDriverByNameUserCase.execute(
            PlacemarkSubscriber(),
            SelectDriverByNameUserCase.Params.forDriverName(name)
        )
    }

    inner class PlacemarksSubscriber : DisposableSingleObserver<List<Placemark>>() {
        override fun onSuccess(t: List<Placemark>) {
            locations.postValue(Resource.success(t.map { mapper.mapToView(it) }))
        }

        override fun onError(e: Throwable) {
            locations.postValue(Resource.error(e.localizedMessage))
        }
    }

    inner class PlacemarkSubscriber : DisposableSingleObserver<Placemark>() {
        override fun onSuccess(t: Placemark) {
            placemark.postValue(Resource.success(mapper.mapToView(t)))
        }

        override fun onError(e: Throwable) {
            placemark.postValue(Resource.error(ERROR_MESSAGE))
        }
    }
}

