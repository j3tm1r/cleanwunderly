package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map

interface MapClickListener {
  fun onDriverSelected(name: String)
}