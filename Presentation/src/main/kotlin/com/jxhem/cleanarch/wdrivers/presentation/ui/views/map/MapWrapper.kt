package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map

import com.jxhem.cleanarch.wdrivers.presentation.Model
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state.MapState

interface MapWrapper {
  fun updateUsersLocation(location: Model.CoordinatesView)
  fun renderState(state: MapState)
}