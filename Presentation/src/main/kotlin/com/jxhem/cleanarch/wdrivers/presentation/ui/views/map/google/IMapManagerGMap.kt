package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.google

import android.content.Context
import android.view.LayoutInflater
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.jxhem.cleanarch.wdrivers.R
import com.jxhem.cleanarch.wdrivers.presentation.Model
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.MapClickListener
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.MapWrapper
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.google.clustering.PlacemarkClusterRenderer
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.google.clustering.PlacemarkInfoWindowAdapter
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.google.clustering.PlacemarkItem
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state.MapState
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state.MapState.Companion.NO_USER

class IMapManagerGMap(
    private val googleMap: GoogleMap,
    private val context: Context,
    private val listener: MapClickListener
) : MapWrapper {

    private val clusterManager: ClusterManager<PlacemarkItem> = ClusterManager(context, googleMap)
    private var marker: Marker? = null
    private val placemarksInfoAdapter = PlacemarkInfoWindowAdapter(LayoutInflater.from(context))

    init {
        clusterManager.markerCollection.setOnInfoWindowAdapter(placemarksInfoAdapter)
        clusterManager.renderer = PlacemarkClusterRenderer(context, googleMap, clusterManager)

        googleMap.setInfoWindowAdapter(clusterManager.markerManager)
        googleMap.uiSettings.isZoomControlsEnabled = true
        googleMap.uiSettings.isMapToolbarEnabled = false

        clusterManager.setOnClusterItemClickListener {
            if (it != null) listener.onDriverSelected(it.title)
            true
        }

        googleMap.setOnMarkerClickListener(clusterManager)
        googleMap.setOnCameraIdleListener(clusterManager)
    }


    override fun renderState(state: MapState) {
        val placemarks = state.getPlacemarks().map { PlacemarkItem(it) }
        loadPlacemarks(placemarks)
        if (NO_USER == state.getDriversName()) {
            showPlacemarks(state.getPlacemarks())
        } else {
            showSinglePlacemark(placemarks[0])
        }

    }

    private fun showSinglePlacemark(placeMark: PlacemarkItem) {
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(placeMark.position))
        showMarkerInfoWindow()

        googleMap.setOnCameraIdleListener {
            showMarkerInfoWindow()
            clusterManager.onCameraIdle()
        }
    }

    private fun showMarkerInfoWindow() {
        clusterManager.markerCollection.markers.forEach {
            it.showInfoWindow()
        }
    }

    private fun showPlacemarks(placemarks: List<Model.PlaceMarkView>) {
        var latTotal = 0f
        var lonTotal = 0f

        placemarks.forEach {
            latTotal += it.coordinates.lat
            lonTotal += it.coordinates.lon
        }

        val location = LatLng(
            (latTotal / placemarks.size).toDouble(),
            (lonTotal / placemarks.size).toDouble()
        )

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, ZOOM_LEVEL))
        googleMap.setOnCameraIdleListener(clusterManager)
    }

    override fun updateUsersLocation(location: Model.CoordinatesView) {
        // we remove the previous marker if it exists
        marker?.remove()
        // we create a new marker
        marker = googleMap.addMarker(
            MarkerOptions()
                .position(LatLng(location.lat.toDouble(), location.lon.toDouble()))
                .zIndex(2f)
                .title(context.getString(R.string.users_location_label))
        )
        // the marker is eventually added to the map
        marker?.showInfoWindow()
    }

    private fun loadPlacemarks(placemarks: List<PlacemarkItem>) {
        clusterManager.clearItems()
        clusterManager.cluster()
        clusterManager.addItems(placemarks)
        clusterManager.cluster()
    }

    companion object {
        private const val ZOOM_LEVEL: Float = 10f
    }
}