package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.google.clustering

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.jxhem.cleanarch.wdrivers.R


class PlacemarkClusterRenderer(
    context: Context?,
    googleMap: GoogleMap,
    clusterManager: ClusterManager<PlacemarkItem>
) : DefaultClusterRenderer<PlacemarkItem>(context, googleMap, clusterManager) {

    private val bitmapDescriptor: BitmapDescriptor =
        BitmapDescriptorFactory.fromResource(R.drawable.pin_car_green)

    override fun onBeforeClusterItemRendered(
        item: PlacemarkItem,
        markerOptions: MarkerOptions
    ) {
        markerOptions.icon(bitmapDescriptor)
    }
}