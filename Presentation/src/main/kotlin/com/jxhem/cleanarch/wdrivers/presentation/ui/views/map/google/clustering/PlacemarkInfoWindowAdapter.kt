package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.google.clustering

import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.jxhem.cleanarch.wdrivers.R


class PlacemarkInfoWindowAdapter(private val layoutInflater: LayoutInflater) :
  GoogleMap.InfoWindowAdapter {
  override fun getInfoContents(marker: Marker?): View {
    val popup = layoutInflater.inflate(R.layout.info_window_layout, null)

    (popup.findViewById(R.id.title) as TextView).text = marker?.snippet

    return popup
  }

  override fun getInfoWindow(marker: Marker?): View {
    val popup = layoutInflater.inflate(R.layout.info_window_layout, null)

    (popup.findViewById(R.id.title) as TextView).text = marker?.snippet

    return popup
  }
}