package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.google.clustering

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem
import com.jxhem.cleanarch.wdrivers.presentation.Model

class PlacemarkItem(
    private val placemark: Model.PlaceMarkView
) : ClusterItem {
    override fun getSnippet(): String {
        return placemark.name
    }

    override fun getTitle(): String {
        return placemark.name
    }

    override fun getPosition(): LatLng {
        return LatLng(placemark.getLat(), placemark.getLon())
    }
}