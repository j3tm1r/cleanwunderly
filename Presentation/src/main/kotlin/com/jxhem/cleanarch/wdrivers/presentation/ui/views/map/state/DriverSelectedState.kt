package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state

import com.jxhem.cleanarch.wdrivers.presentation.Model

class DriverSelectedState(
    private val driversName: String,
    private val placemarksList: List<Model.PlaceMarkView>
) : MapState {

    override fun getDriversName(): String = driversName

    override fun getPlacemarks(): List<Model.PlaceMarkView> = placemarksList
}