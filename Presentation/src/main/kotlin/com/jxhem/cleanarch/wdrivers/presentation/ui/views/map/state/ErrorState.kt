package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state

import com.jxhem.cleanarch.wdrivers.presentation.Model

class ErrorState(private val message: String) : MapState {

    fun getMessage() = message

    override fun getDriversName() = MapState.NO_USER

    override fun getPlacemarks(): List<Model.PlaceMarkView> =
        throw UnsupportedOperationException("Not needed here")
}