package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state

import com.jxhem.cleanarch.wdrivers.presentation.Model

interface MapState {
    fun getDriversName(): String
    fun getPlacemarks(): List<Model.PlaceMarkView>

    companion object {
        const val NO_USER: String = "___NO_USER___"
    }

}