package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state


import com.jxhem.cleanarch.wdrivers.presentation.Model
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state.MapState.Companion.NO_USER

class NoDriverSelectedState(
    private val placemarksList: List<Model.PlaceMarkView>
) : MapState {
    override fun getDriversName(): String = NO_USER

    override fun getPlacemarks(): List<Model.PlaceMarkView> = placemarksList
}