package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.strategy

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import com.jxhem.cleanarch.wdrivers.presentation.Model
import com.jxhem.cleanarch.wdrivers.presentation.state.Resource
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state.MapState

abstract class MapStrategy(
    protected val userSelectedSource: MutableLiveData<Resource<Model.PlaceMarkView>>,
    protected val placemarksSource: MutableLiveData<Resource<List<Model.PlaceMarkView>>>,
    protected val mapStateSource: MediatorLiveData<MapState>
) {
    protected abstract fun setupStrategy()

    init {
        this.setupStrategy()
    }
}