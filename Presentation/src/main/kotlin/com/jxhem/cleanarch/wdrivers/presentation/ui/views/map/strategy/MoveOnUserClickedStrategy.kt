package com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.strategy

import android.arch.lifecycle.MediatorLiveData
import android.arch.lifecycle.MutableLiveData
import com.jxhem.cleanarch.wdrivers.presentation.Model
import com.jxhem.cleanarch.wdrivers.presentation.state.Resource
import com.jxhem.cleanarch.wdrivers.presentation.state.ResourceState
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.map.state.*

class MoveOnUserClickedStrategy private constructor(
    userSelectedSource: MutableLiveData<Resource<Model.PlaceMarkView>>,
    placemarksSource: MutableLiveData<Resource<List<Model.PlaceMarkView>>>,
    mapStateSource: MediatorLiveData<MapState>
) : MapStrategy(userSelectedSource, placemarksSource, mapStateSource) {

    private var oldState: MapState? = null

    override fun setupStrategy() {
        oldState = mapStateSource.value
        mapStateSource.addSource(placemarksSource) {
            when {
                ResourceState.SUCCESS == it?.status -> {
                    if (oldState == null) mapStateSource.postValue(NoDriverSelectedState(it.data!!))
                    mapStateSource.removeSource(placemarksSource)
                }
                ResourceState.LOADING == it?.status -> mapStateSource.postValue(LoadingState(it.message!!))
                else -> mapStateSource.postValue(ErrorState(it?.message!!))
            }
        }

        mapStateSource.removeSource(userSelectedSource)
        mapStateSource.addSource(userSelectedSource) { resourcePV ->
            if (resourcePV?.status == ResourceState.SUCCESS) {
                // when we restore te window we are going to have an old state here, getting rid of it
                if (oldState != null) oldState = null
                else when (mapStateSource.value) {
                    is NoDriverSelectedState -> {
                        mapStateSource.postValue(
                            DriverSelectedState(
                                resourcePV.data?.name!!,
                                placemarksSource.value?.data?.filter { it.name == resourcePV.data.name }!!
                            )
                        )
                    }
                    is DriverSelectedState -> {
                        mapStateSource.postValue(NoDriverSelectedState(placemarksSource.value?.data!!))
                    }
                }
            }
        }
    }

    class Builder {
        private lateinit var userSelectedSource: MutableLiveData<Resource<Model.PlaceMarkView>>
        private lateinit var locationsSource: MutableLiveData<Resource<List<Model.PlaceMarkView>>>
        private lateinit var mapStateSource: MediatorLiveData<MapState>

        fun build(): MapStrategy =
            MoveOnUserClickedStrategy(userSelectedSource, locationsSource, mapStateSource)

        fun setUserSelectedSource(userSelected: MutableLiveData<Resource<Model.PlaceMarkView>>): Builder {
            this.userSelectedSource = userSelected
            return this
        }

        fun setPlacemarksSource(locationsS: MutableLiveData<Resource<List<Model.PlaceMarkView>>>): Builder {
            this.locationsSource = locationsS
            return this
        }

        fun setStateHolder(mapState: MediatorLiveData<MapState>): Builder {
            this.mapStateSource = mapState
            return this
        }
    }

}