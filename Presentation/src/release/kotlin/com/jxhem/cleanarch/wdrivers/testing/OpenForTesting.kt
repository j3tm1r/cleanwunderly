package com.jxhem.cleanarch.wdrivers.testing


@Target(AnnotationTarget.CLASS)
annotation class OpenForTesting
