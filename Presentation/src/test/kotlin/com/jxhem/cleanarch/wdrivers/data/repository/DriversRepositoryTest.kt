package com.jxhem.cleanarch.wdrivers.data.repository

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import com.jxhem.cleanarch.wdrivers.presentation.android.livedata.ApiResponse
import com.jxhem.cleanarch.wdrivers.presentation.android.livedata.LiveDataCallAdapterFactory
import com.jxhem.cleanarch.wdrivers.data.net.LocationsApi
import com.jxhem.cleanarch.wdrivers.data.net.Model
import com.jxhem.cleanarch.wdrivers.data.persistence.WDriversDAO
import com.jxhem.cleanarch.wdrivers.data.persistence.WdriversDBModel
import com.jxhem.cleanarch.wdrivers.data.source.DataSource
import com.jxhem.cleanarch.wdrivers.data.source.DriversRepository
import com.jxhem.cleanarch.wdrivers.util.InstantAppExecutors
import com.jxhem.persistence.util.LiveDataTestUtil.getValue
import com.jxhem.cleanarch.wdrivers.util.mock
import junit.framework.Assert.assertNotNull
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Okio
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.*
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection

@RunWith(JUnit4::class)
class DriversRepositoryTest {

    private val mockedDriversDao: WDriversDAO = mock()
    private val mockedLocationsApi: LocationsApi = mock()

    private lateinit var driversRepository: DataSource
    private val liveDataHolder: MutableLiveData<List<WdriversDBModel.DBPlaceMark>> =
        MutableLiveData()

    private lateinit var mockWebServer: MockWebServer
    private lateinit var locationsApi: LocationsApi

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()


    @Before
    fun setup() {
        doReturn(liveDataHolder).`when`(mockedDriversDao).loadPlacemarks()
        doReturn(
            MutableLiveData<ApiResponse<Model.PlacemarksHolder>>()
        ).`when`(mockedLocationsApi).getLocations()


        mockWebServer = MockWebServer()

        driversRepository =
                DriversRepository(mockedLocationsApi, mockedDriversDao, InstantAppExecutors())
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    private fun bringUpMockServer() {

        locationsApi = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(LiveDataCallAdapterFactory())
            .build()
            .create(LocationsApi::class.java)

        driversRepository =
                DriversRepository(locationsApi!!, mockedDriversDao, InstantAppExecutors())
    }

    @Test
    fun locationsApiIsCalled() {
        liveDataHolder.postValue(emptyList())
        driversRepository.getLocations().observeForever(mock())
        verify(mockedDriversDao).loadPlacemarks()
        verify(mockedLocationsApi).getLocations()
    }

    @Test
    fun locationsApiIsNotCalledWhenUsersAlreadyInDb() {
        liveDataHolder.postValue(
            listOf(
                WdriversDBModel.DBPlaceMark(
                    name = "",
                    interior = "",
                    fuel = 1,
                    exterior = "",
                    engineType = "",
                    coordinates = WdriversDBModel.Coordinates(0f, 0f, 0f),
                    address = "",
                    vin = ""
                )
            )
        )
        driversRepository.getLocations().observeForever(mock())
        verify(mockedDriversDao).loadPlacemarks()
        verify(mockedLocationsApi, never()).getLocations()
    }

    @Test
    fun placemarksAreCorrectlyLoaded() {
        // Given
        liveDataHolder.postValue(emptyList())
        bringUpMockServer()
        enqueueResponse("/wunderbucket/locations.json")

        //When
        driversRepository.getLocations().observeForever(mock())
        verify(mockedDriversDao).loadPlacemarks()

        val request = mockWebServer.takeRequest()
        MatcherAssert.assertThat(request?.path, `is`("/wunderbucket/locations.json"))

        val placemarksList = getValue(driversRepository.getLocations()).data

        assertNotNull(placemarksList)

        MatcherAssert.assertThat(
            placemarksList!![0].address,
            CoreMatchers.`is`("Lesserstraße 170, 22049 Hamburg")
        )
        MatcherAssert.assertThat(
            placemarksList[4].vin,
            CoreMatchers.`is`("WME4513341K412721")
        )
    }

    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader
            .getResourceAsStream("api-response/$fileName")
        val source = Okio.buffer(Okio.source(inputStream))
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(
            mockResponse
                .setResponseCode(HttpURLConnection.HTTP_OK)
                .setBody(source.readString(Charsets.UTF_8))

        )
    }
}