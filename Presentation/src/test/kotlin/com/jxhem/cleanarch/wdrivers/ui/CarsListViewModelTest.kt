package com.jxhem.cleanarch.wdrivers.ui

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.MutableLiveData
import com.jxhem.cleanarch.wdrivers.data.persistence.WdriversDBModel
import com.jxhem.cleanarch.wdrivers.data.source.DataSource
import com.jxhem.cleanarch.wdrivers.data.source.Resource
import com.jxhem.cleanarch.wdrivers.presentation.ui.views.carslist.CarsListViewModel
import com.jxhem.cleanarch.wdrivers.util.mock
import junit.framework.Assert.assertEquals
import org.hamcrest.CoreMatchers.notNullValue
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mockito.*


@RunWith(JUnit4::class)
class CarsListViewModelTest {


    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private var repository: DataSource = mock(DataSource::class.java)
    private lateinit var carsListViewModel: CarsListViewModel

    private lateinit var liveData: MutableLiveData<Resource<List<WdriversDBModel.DBPlaceMark>>>

    @Before
    fun setUp() {
        liveData = MutableLiveData()

        doReturn(liveData).`when`(repository).getLocations()

        carsListViewModel = CarsListViewModel(repository)
    }

    @After
    fun cleanUp() {
        reset(repository)
    }

    @Test
    fun testNotNull() {
        assertThat(carsListViewModel.getPlacemarks(), notNullValue())
    }

    private lateinit var listOfPlacemarks: Resource<List<WdriversDBModel.DBPlaceMark>>


    @Test
    fun errorIsReported() {
        val errorMesage = "Error loading data"
        liveData.postValue(Resource.error(errorMesage))

        // When
        carsListViewModel.getPlacemarks().observeForever(mock())

        // Then
        verify(repository).getLocations()
        assertEquals(
            Resource.error<List<WdriversDBModel.DBPlaceMark>>(errorMesage),
            carsListViewModel.getPlacemarks().value
        )
    }

    @Test
    fun testPlacemarksDoLoad() {
        addTestCases()
        carsListViewModel.getPlacemarks().observeForever(mock())
        verify(repository).getLocations()
        assertEquals(
            listOfPlacemarks,
            carsListViewModel.getPlacemarks().value
        )
    }

    private fun addTestCases() {

        listOfPlacemarks = Resource.success(
            listOf(
                WdriversDBModel.DBPlaceMark(
                    address = "",
                    coordinates = WdriversDBModel.Coordinates(0f, 0f, 0f),
                    name = "",
                    engineType = "",
                    exterior = "",
                    fuel = 1,
                    interior = "",
                    vin = ""
                ),
                WdriversDBModel.DBPlaceMark(
                    address = "",
                    coordinates = WdriversDBModel.Coordinates(0f, 0f, 0f),
                    name = "",
                    engineType = "",
                    exterior = "",
                    fuel = 1,
                    interior = "",
                    vin = ""
                ),
                WdriversDBModel.DBPlaceMark(
                    address = "",
                    coordinates = WdriversDBModel.Coordinates(0f, 0f, 0f),
                    name = "",
                    engineType = "",
                    exterior = "",
                    fuel = 1,
                    interior = "",
                    vin = ""
                )
            )
        )
        liveData.postValue(
            listOfPlacemarks
        )
    }


}