package com.jxhem.cleanarch.wdrivers.remote

import com.jxhem.cleanarch.wdrivers.remote.net.LocationsApi
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NetModule {

  fun provideLocationsApi(): LocationsApi {
    return provideRetrofit(
      provideBaseUrl(),
      provideOkHttpClient()
    )
      .create(LocationsApi::class.java)
  }

  private fun provideRetrofit(baseUrl: HttpUrl, okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
      .client(okHttpClient)
      .baseUrl(baseUrl)
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
      .build()
  }

  private fun provideBaseUrl(): HttpUrl {
    return PRODUCTION_API_URL
  }

  private fun provideOkHttpClient(): OkHttpClient {
    return createApiClient()
      .connectTimeout(30, TimeUnit.SECONDS)
      .readTimeout(30, TimeUnit.SECONDS)
      // setup cache .cache(directory, size)
      //add interceptors / authenticators here
      .build()
  }

  private fun createApiClient(): OkHttpClient.Builder {
    return OkHttpClient.Builder()
  }

  val PRODUCTION_API_URL: HttpUrl = HttpUrl.parse(LocationsApi.BASE_URL)!!
}