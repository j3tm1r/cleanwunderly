package com.jxhem.cleanarch.wdrivers.remote

import com.jxhem.cleanarch.wdrivers.data.model.PlacemarkEntity
import com.jxhem.cleanarch.wdrivers.data.repository.RemoteSource
import com.jxhem.cleanarch.wdrivers.remote.mapper.RemoteMapper
import com.jxhem.cleanarch.wdrivers.remote.net.LocationsApi
import io.reactivex.Single
import javax.inject.Inject

class RemotePlacemarksSource @Inject constructor(
  private val locationsApi: LocationsApi,
  private val remoteMapper: RemoteMapper
) : RemoteSource {
  override fun loadPlacemarks(): Single<List<PlacemarkEntity>> =
    locationsApi.getLocations()
      .map {
        it.placemarks.map {
          remoteMapper.mapFromModel(it)
        }
      }
}