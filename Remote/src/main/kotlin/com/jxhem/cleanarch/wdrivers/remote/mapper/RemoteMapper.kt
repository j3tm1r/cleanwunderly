package com.jxhem.cleanarch.wdrivers.remote.mapper

import com.jxhem.cleanarch.wdrivers.data.model.CoordinatesEntity
import com.jxhem.cleanarch.wdrivers.data.model.PlacemarkEntity
import com.jxhem.cleanarch.wdrivers.remote.net.Model


class RemoteMapper :
  ModelMapper<Model.Placemark, PlacemarkEntity> {
  override fun mapFromModel(model: Model.Placemark): PlacemarkEntity =
    PlacemarkEntity(
      address = model.address,
      name = model.name,
      coordinates = CoordinatesEntity(
        model.coordinates[1],
        model.coordinates[0],
        model.coordinates[2]
      ),
      engineType = model.engineType,
      interior = model.interior,
      exterior = model.exterior,
      fuel = model.fuel,
      vin = model.vin
    )
}