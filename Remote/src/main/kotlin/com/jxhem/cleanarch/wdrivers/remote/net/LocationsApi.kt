package com.jxhem.cleanarch.wdrivers.remote.net

import io.reactivex.Single
import retrofit2.http.GET

interface LocationsApi {

  @GET("/wunderbucket/locations.json")
  fun getLocations(): Single<Model.PlacemarksHolder>

  companion object {

    val BASE_URL = "https://s3-us-west-2.amazonaws.com"
  }
}