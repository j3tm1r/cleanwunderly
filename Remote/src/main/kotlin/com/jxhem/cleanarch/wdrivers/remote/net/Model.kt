package com.jxhem.cleanarch.wdrivers.remote.net

object Model {

  data class PlacemarksHolder(
    val placemarks: List<Placemark>
  )

  data class Placemark(
    val address: String,
    val coordinates: List<Float>,
    val engineType: String,
    val exterior: String,
    val interior: String,
    val fuel: Int,
    val name: String,
    val vin: String
  )
}
